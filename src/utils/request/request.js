// 原生js简单实现ajax
export function ajax(params) {
  params = params || {};
  params.data = params.data || {};
  // 判断是ajax请求还是JSONP请求
  var jsonType = params.jsonp ? jsonp(params): json(params);

  function json(params) {
    var type = params.type || 'GET'.toUpperCase();
    // 避免有特殊字符，必须格式化
    params.data = formatParams(params.data);
    var xhr = null;
    if (window.XMLHttpRequest){
      // 如果存在XMLHttpRequest方法
      xhr = new XMLHttpRequest();
    }else {
      // ie6及以下版本
      xhr = new ActiveXObject('Microsoft.XMLHTTP');
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4) {
        // xhr.readyState为4的时候已经接收到全部数据
        var status = xhr.status;
        if (status == 200) {
          var response = '';
          // 判断接收数据的内容类型
          var type = xhr.getResponseHeader('Content-type');
          if (type.indexOf('xml') !== -1 && xhr.responseXML) {
            response = xhr.responseXML; // xml对象响应
          } else if (type === 'application/json') {
            response = JSON.parse(xhr.responseText); // json对象响应
          }else {
            response = xhr.responseText; // 字符串响应
          }
          // 成功回调函数
          params.success && params.success(response);
        } else {
          params.error && params.error();
        }
      }
    }

    // 传输数据
    if (params.type === 'GET'){// 如果是GET方法，就把传输的数据放在url后面
      xhr.open(params.type, params.url + '?' + params.data, true);
      xhr.send(null);
    } else {
      // 如果是POST方法，就把传输的数据通过send方法传送
      xhr.open(params.type, params.url, true);
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
      // 传输数据
      xhr.send(params.data);
    }
  }
  // jsonp请求
  function jsonp(params) {
    //创建script标签并加入到页面中
    var callbackName = params.jsonp;
    var head = document.getElementsByTagName('head')[0];
    // 设置传递给后台的回调参数名
    params.data['callback'] = callbackName;
    var data = formatParams(params.data);
    var script = document.createElement('script');
    head.appendChild(script);

    //创建jsonp回调函数
    window[callbackName] = function(json) {
      head.removeChild(script);
      clearTimeout(script.timer);
      window[callbackName] = null;
      params.success && params.success(json);
    };


    //发送请求
    script.src = params.url + '?' + data;


    //为了得知此次请求是否成功，设置超时处理
    if(params.time) {
      script.timer = setTimeout(function() {
        window[callbackName] = null;
        head.removeChild(script);
        params.error && params.error({
          message: '超时'
        });
      }, time);
    }
  }

  function formatParams(data) {// 对传输的数据进行编码
    var arr = [];
    for (var key in data) {
      // encodeURIComponent() 用于对URI中的某一部分进行编码
      arr.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    }
    // 添加一个随机参数，防止缓存

    return arr.join('&');
  }

  function random() {
    return Math.floor(Math.random() * 10000 + 500);
  }
}
