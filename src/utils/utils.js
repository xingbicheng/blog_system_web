export const methods = {
  dom: {}
}

methods.dom.appendChildren = function (node, arr) {
  arr.forEach((item) => {
    node.appendChild(item)
  })
}