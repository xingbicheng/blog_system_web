import { blockData } from "../../../mock/api"
import { methods } from "../../utils/utils"

export function Block(config) {
  const { view, title, user_id } = config;
  var blockNode = document.createElement('div');
  var titleNode = document.createElement('div');
  var userNode = document.createElement('div');
  var viewNode = document.createElement('div');
  titleNode.innerText = title;
  userNode.innerText = user_id;
  viewNode.innerText = view;
  methods.dom.appendChildren(blockNode, [titleNode, userNode, viewNode]);
  return blockNode;
}