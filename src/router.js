import { blogTest } from "./test/test"
import { index } from "./pages/index"

function Router() {
  this.currentHash = '/';
  this.routes = {
    '/test': blogTest,
    '/index': index
  };
}

// 设置路由
Router.prototype.setRoute = function (path, callback) {
  this.routes[path] = callback || function () {
    console.error('该路径不存在')
  }
}

// 刷新页面函数，即跳转路由
Router.prototype.refresh = function () {
  this.currentHash = location.hash.slice(1) || '/';
  console.log(this.currentHash);
  this.routes[this.currentHash]();
}

// 初始页面，给页面添加监听器，load事件或hashchange事件发生则调用route相应的方法
Router.prototype.init = function () {
  var that = this;
  window.addEventListener('load', function () {
    that.refresh();
  }, false);
  window.addEventListener('hashchange', function () {
    that.refresh();
  }, false);
}

export default Router;
