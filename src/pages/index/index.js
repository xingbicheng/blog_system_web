import { ajax } from "../../utils/request/request"
import { Block } from "../../components/Block/Block"

export async function index() {
  function getData(callback) {
    ajax({
      url: 'http://127.0.0.1:8362/content/list',
      type: 'POST',
      data: {},
      success: function (res) {
        callback(JSON.parse(res).data);
      },
      error: function (error) {
        console.log('somewhere has a error');
      }
    });
  }

  function operating(json) { // 问题1 为何能在该函数中操作异步数据 ？
    json.forEach(function (item) {
      const container = document.getElementById('container');
      var block = Block(item)
      container.appendChild(block);
    })
  }

  getData(operating);
}