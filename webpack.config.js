const path = require('path');
const webpack = require('webpack');
const webpackDevServer = require('webpack-dev-server');
const htmlWebpackPlugin = require('html-webpack-plugin')
// dirname为获得当前执行文件所在目录的完整目录名
// 改配置实现的文件的热更新

module.exports = {
  entry: __dirname + '/src/app.js',//入口
  output: {//出口
    path: __dirname + '/dist/',//路径
    filename: 'test.bundle.js'//打包后的生成文件名
  },
  module: {
    rules: [//webpack的处理规则，是一个数组，可以包含多条规则
      {
        test: /.\js$/,//正则表达式，可以匹配需要处理的文件(以.js结尾的文件)
        include: __dirname + '/src/',//匹配的路径，即只在该路径下匹配文件，可以优化webpack处理的时间
        exclude: __dirname + '/node_modules/',//跳过的路径，即不匹配的路径，可以优化webpack处理的时间
        loader: "babel-loader"//处理匹配到的文件的插件
      }
    ]
  },
  devServer: {
    contentBase: "./dist",
    historyApiFallback: true,
    inline: true,
    progress: true,
    hot: true,
    port: 8080
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new htmlWebpackPlugin({
      template: './src/templete.html'
    })
  ],
}